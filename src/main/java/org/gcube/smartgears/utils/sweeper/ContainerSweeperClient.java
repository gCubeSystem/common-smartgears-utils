package org.gcube.smartgears.utils.sweeper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author andrea
 *
 */
public class ContainerSweeperClient {
	
	public static void main (String args[]) {
		
		String savedTokenFileName= null;
		String ghnHome = null;
		
		if (args.length>0) 
			for (String arg: args) {
				if (arg.startsWith("-G"))
					ghnHome = arg.replaceAll("-G(.*)$", "$1");
				else savedTokenFileName = arg;
			}
		
		if (ghnHome==null)
			ghnHome = System.getenv("GHN_HOME");;
		
		Logger logger = LoggerFactory.getLogger(ContainerSweeperClient.class);
		
		Sweeper sw = null;
		try {
			sw = new Sweeper(ghnHome);
		} catch (Exception e) {
			System.out.println("Error initializing the Sweeper");
			e.printStackTrace();
			logger.error("Error initializing the Sweeper ", e);
			System.exit(1);
		}
							
		try {
			if (savedTokenFileName!=null){
				sw.saveTokens(savedTokenFileName);
				logger.info("file saved on Smartgears directory with name {} ",savedTokenFileName);
			}
			sw.forceDeleteHostingNode();
		} catch (Exception e) {
			logger.error("Error cleaning the  GHN profile ", e);
			System.exit(1);
		}
		
		
	}

}
