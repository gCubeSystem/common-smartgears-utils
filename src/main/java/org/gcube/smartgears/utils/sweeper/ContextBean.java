package org.gcube.smartgears.utils.sweeper;

public class ContextBean {
	
	private String token;
	private String context;

	public ContextBean(String token, String context) {
		super();
		this.token = token;
		this.context = context;
	}
	
	public String getToken() {
		return token;
	}
	
	public String getContext() {
		return context;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContextBean other = (ContextBean) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}
	
	
}
