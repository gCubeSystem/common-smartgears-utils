package org.gcube.smartgears.utils.sweeper.test;



import java.util.List;

import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.gcube.smartgears.utils.sweeper.Sweeper;
import org.junit.Before;
import org.junit.Test;

public class SweeperTest {
	
	Sweeper sw = null;
	
	@Before
	public void setUp(){
		try {
			sw = new Sweeper("/home/lucio/Smartgears");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void deleteHNs() {
		sw.forceDeleteHostingNode();
	}
	
	@Test
	public void deleteFromScope() {
		
		RegistryPublisher rp=RegistryPublisherFactory.create();
		ScopeProvider.instance.set("/gcube/devNext/NextNext");
		DiscoveryClient<HostingNode> client = ICFactory.clientFor(HostingNode.class);
		SimpleQuery query = ICFactory.queryFor(HostingNode.class);
		query.addCondition("$resource/ID/text() = 'c8201474-a8cd-440f-9f62-a1e1b107ceb6'");
		List<HostingNode> nodes = client.submit(query);
		rp.remove(nodes.get(0));
	}
}
